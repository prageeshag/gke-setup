BASEDIR = $(shell pwd)
include Makefile.properties

#Setup a gke regional cluster
cluster.up: 
	-gcloud container clusters create $(CLUSTER) --region $(REGION) \
--node-locations=$(ZONES) 

#Delete the gke cluster
cluster.clean: 
	-gcloud container clusters delete $(CLUSTER) --region=$(REGION)

#Get cluster details
cluster.details:
	-gcloud container clusters list
	-gcloud container clusters describe $(CLUSTER) --region=$(REGION)

#Build a simple php application
myapp.build:
	-gcloud builds submit --config my-app/cloudbuild.yaml

#get pod list
myapp.pods:
	-kubectl get pods

# Build the image only
myapp.build_image:	
	-gcloud builds submit --config my-app/myapp-update.yaml

#Build the app image only and update the deployment
myapp.update:
	-kubectl set image deployment $(MYAPP) $(MYAPP)=gcr.io/$(PROJECT)/$(MYAPP):$(TAG)


#Crate a http loadbalancer to the app
ingress:
	-kubectl create -f my-app/yaml/ingress.yaml

#Delete ingress 
ingress.clean:
	-kubectl delete -f my-app/yaml/ingress.yaml

#Find the ip of the service
ingress.getip:
	- kubectl get ingress $(MYINGRESS)

#Remove the php app and the http ingress
myapp.clean:
	-kubectl delete -f my-app/yaml/dp-app.yaml

## Clean all
clean.all: myapp.clean ingress.clean cluster.clean