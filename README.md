#  Setup GKE cluster and deploy a simple app

## Tasks
 - Setup regional cluster,
 - Create glo
 - dbancer,
 - Setup cicd in cluster,
 - Deploy the sample app to cluster through cicd,
 - Application should be accessed through GLB.

### How to create a regional cluster
This will create a gke regional cluster with 3 nodes in regions and zones which are mentioned in Makefile.properties. This takes few minutes to complete
```sh
$ make cluster.up
```

### Get cluster details
This will list the clusters and the deatils of created cluster
```sh
$ make cluster.details
```

### Build and deploy simple php hello world application 
This will clone the git repository and build the dp-app docker image.
```sh
$ make myapp.build
```

### Check pod status
```sh
$ make myapp.pods
```

### Create global load balancer to access the application
This will take serveral minutes to update and give you an IP.
```sh
$ make ingress
```

### Determine the ip to access the app
You can access the app using the ip.
```sh
$ make ingress.getip
```


### Update the app image after a change.(Optional)
update the code in git and push into the master branch. and update the my-app/myapp-update.yaml with the image version
```sh
$ make myapp.build_image
```


### Deploy updated image (Optional)
Change the tag version in Makefile,properties (TAG) file to deploy to that image
```sh
$ make myapp.update
```

### Delete all
Change the tag version in Makefile,properties (TAG) file to deploy to that image
```sh
$ make clean.all
```